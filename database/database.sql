-- NOMBRE BDD pg-covid-delivery

CREATE TABLE ROLES(
  ID_ROL SERIAL NOT NULL,
  DESCRIPTION VARCHAR(30) NOT NULL,
  CONSTRAINT PK_ID_ROL PRIMARY KEY (ID_ROL)
);

CREATE TABLE STATE(
  ID_STATE SERIAL NOT NULL,
  DESCRIPTION VARCHAR(30) NOT NULL,
  CONSTRAINT PK_ID_STATE PRIMARY KEY (ID_STATE)
);

CREATE TABLE TYPE_BUSINESS(
  ID_TYPE_BUSINESS SERIAL NOT NULL,
  DESCRIPTION VARCHAR(30) NOT NULL,
  CONSTRAINT PK_ID_TYPE_BUSINESS PRIMARY KEY (ID_TYPE_BUSINESS)
);

CREATE TABLE USERS(
  ID_USER SERIAL NOT NULL,
  NAMES VARCHAR(50),
  LASTNAME VARCHAR(50),
  NICKNAME VARCHAR(50),
  AGE INT,
  EMAIL VARCHAR(100) NOT NULL,
  DATE_CREATION DATE DEFAULT CURRENT_DATE NOT NULL,
  TIME_CREATION TIME DEFAULT CURRENT_TIME NOT NULL,
  PASSWORD VARCHAR(200) NOT NULL,
  ID_ROL INT NOT NULL REFERENCES ROLES,
  ID_STATE INT NOT NULL REFERENCES STATE,
  ID_TYPE_BUSINESS INT REFERENCES TYPE_BUSINESS,
  CONSTRAINT PK_ID_USER PRIMARY KEY (ID_USER)
);

CREATE TABLE BUSINESS(
  ID_BUSINESS SERIAL NOT NULL,
  ID_USER INT NOT NULL REFERENCES USERS,
  NAME VARCHAR(100) NOT NULL,
  LATITUD VARCHAR(100) NOT NULL,
	EMAIL VARCHAR(100) NOT NULL,
  LONGITUD VARCHAR(100) NOT NULL,
  DESCRIPTION TEXT NOT NULL,
  CONSTRAINT PK_ID_BUSINESS PRIMARY KEY(ID_BUSINESS)
);

CREATE TABLE PEDIDO(
  ID_PEDIDO SERIAL NOT NULL,
  ID_USER INT NOT NULL REFERENCES USERS,
  ID_BUSINESS INT NOT NULL REFERENCES BUSINESS,
  DESCRIPTION VARCHAR(200) NOT NULL,
	PRICE NUMERIC(10,2) NOT NULL DEFAULT 0.00,
  CONSTRAINT PK_ID_PEDIDO PRIMARY KEY(ID_PEDIDO)
);

CREATE TABLE DETALLE_PEDIDO (
  ID_DETALLE_PEDIDO SERIAL NOT NULL,
  ID_PEDIDO INT REFERENCES PEDIDO,
  ARTICLE VARCHAR(200) NOT NULL,
	PRICE NUMERIC(10,2) NOT NULL DEFAULT 0.00,
	UNIT_PRICE NUMERIC(10,2) NOT NULL DEFAULT 0.00,
	QUANTITY INT NOT NULL DEFAULT 0;
	URL INT NOT NULL DEFAULT '';
  VERIFY SMALLINT,
  CONSTRAINT PK_ID_DETALLE_PEDIDO PRIMARY KEY (ID_DETALLE_PEDIDO)
);

CREATE TABLE products(
ID_PRODUCT SERIAL NOT NULL,
ID_BUSINESS INT NOT NULL REFERENCES BUSINESS,
NAME VARCHAR(100) NOT NULL,
PRICE NUMERIC(10,2) NOT NULL,
URL VARCHAR(200) NOT NULL,
CONSTRAINT PK_ID_PRODUCT PRIMARY KEY (ID_PRODUCT)
);

INSERT INTO roles(description) VALUES('ADMINISTRATOR'),('CLIENT'),('BUSINESS'),('COURIER'),('EMPLOYE');
INSERT INTO state(description) VALUES('ACTIVE'),('BY VERIFY'),('INACTIVE'),('LOCKED'),('DISABLED'),('IN PROCESS'),('SENDED'),('DELIVERED'),('CANCELED');
INSERT INTO type_business(description) VALUES('FERRETERIA'),('FARMACIA'),('TIENDA'),('BAZAR'),('LIBRERIA'),
('RESTAURANT'),('VERDURERIA'),('FLORERIA'),('OTROS');

CREATE OR REPLACE FUNCTION public.fn_list_roles()
RETURNS TABLE(id roles.id_rol%TYPE, v_description roles.description%TYPE) AS $$
BEGIN
	RETURN QUERY
	SELECT id_rol, description FROM roles;
END;
$$LANGUAGE plpgsql;

SELECT * FROM fn_list_roles();


CREATE OR REPLACE FUNCTION public.fn_list_state()
RETURNS TABLE(id state.id_state%TYPE, v_description state.description%TYPE) AS $$
BEGIN
	RETURN QUERY
	SELECT id_state, description FROM state;
END;
$$LANGUAGE plpgsql;

SELECT * FROM fn_list_state();


CREATE OR REPLACE FUNCTION public.fn_add_user
(v_email users.email%TYPE, v_password users.password%TYPE,
v_id_rol users.id_rol%TYPE, v_id_state users.id_state%TYPE)
RETURNS INTEGER AS $$
DECLARE id users.id_user%TYPE;
DECLARE emailCopy users.email%TYPE;
BEGIN
	SELECT email into emailCopy FROM users WHERE email = v_email;
	PERFORM emailCopy;
	IF emailCopy IS NULL THEN
		INSERT INTO users (email, password, id_rol, id_state)
		VALUES(v_email, v_password, v_id_rol, v_id_state)RETURNING id_rol into id;
		RETURN id;
	ELSE
		RETURN -1;
	END IF;
END;
$$LANGUAGE plpgsql;

SELECT fn_add_user('test@gmail.com', '12345', 1, 2);


CREATE OR REPLACE FUNCTION public.fn_list_type_business()
RETURNS TABLE(id type_business.id_type_business%TYPE, v_description type_business.description%TYPE) AS $$
BEGIN
	RETURN QUERY
	SELECT id_type_business, description FROM type_business;
END;
$$LANGUAGE plpgsql;

SELECT * FROM fn_list_type_business();


CREATE OR REPLACE FUNCTION public.fn_login(p_email users.email%TYPE)
RETURNS TABLE(id users.id_user%TYPE, v_names users.names%TYPE, v_lastname users.lastname%TYPE,
v_nickname users.nickname%TYPE,v_age users.age%TYPE, v_email users.email%TYPE, v_password users.password%TYPE,
v_id_rol users.id_rol%TYPE, v_id_state users.id_state%TYPE, v_type_business users.id_type_business%TYPE) AS $$
BEGIN
	RETURN QUERY
	SELECT id_user, names, lastname, nickname, age, email, password, id_rol, id_state, id_type_business
	FROM users
	WHERE email = p_email;
END;
$$LANGUAGE plpgsql;

SELECT * FROM fn_login('test@gmail.com');

CREATE OR REPLACE FUNCTION public.fn_add_business
(v_id_user business.id_user%TYPE, v_name business.name%TYPE,
v_latitud business.latitud%TYPE, v_longitud business.longitud%TYPE,
v_description business.description%TYPE) RETURNS INTEGER AS $$
DECLARE cod_business business.id_business%TYPE;
DECLARE existBusiness business.ID_USER%TYPE;
DECLARE id_rol_user users.id_user%TYPE;
BEGIN
	SELECT id_rol into id_rol_user FROM users WHERE id_user = v_id_user;
	SELECT id_user into existBusiness from business WHERE id_user = v_id_user;

	IF id_rol_user = 1 THEN
		INSERT INTO BUSINESS (ID_USER, NAME, LATITUD, LONGITUD, DESCRIPTION)
		VALUES (v_id_user, v_name, v_latitud, v_longitud, v_description);
		RETURN 1;
	ELSIF existBusiness IS NULL THEN
		INSERT INTO BUSINESS (ID_USER, NAME, LATITUD, LONGITUD, DESCRIPTION)
		VALUES (v_id_user, v_name, v_latitud, v_longitud, v_description);
		RETURN 1;
	ELSE
		RETURN -1;
	END IF;
END;
$$LANGUAGE plpgsql;

SELECT fn_add_business
(38, 'Paquita la del barrio', '-12.180411562298318','-76.97755308572461','Es una pollera que apareció por primera vez en Sherk 2 xd');

CREATE OR REPLACE FUNCTION public.fn_list_business()
RETURNS SETOF BUSINESS AS $$
BEGIN
	RETURN QUERY
	SELECT * FROM business;
END;
$$LANGUAGE plpgsql;

SELECT * FROM fn_list_business();

CREATE OR REPLACE FUNCTION public.fn_add_pedido
(v_id_user pedido.id_user%TYPE,
  v_id_business pedido.id_business%TYPE,
  v_description pedido.description%TYPE,
  v_id_state pedido.id_state%TYPE,
  v_total_price pedido.total_price%TYPE,
  v_email pedido.email%TYPE,
  v_created pedido.created%TYPE)
RETURNS INTEGER AS $$
DECLARE v_id_insert pedido.id_pedido%TYPE;
BEGIN
	INSERT INTO PEDIDO(ID_USER, ID_BUSINESS, DESCRIPTION, ID_STATE, TOTAL_PRICE, CREATED, EMAIL)
	VALUES (v_id_user, v_id_business, v_description, v_id_state, v_total_price, v_created, v_email)
	RETURNING id_pedido into v_id_insert;
	RETURN v_id_insert;
END;
$$LANGUAGE plpgsql;

SELECT fn_add_pedido(63, 23, 'Pedido de prueba', 6, 1000.99);

CREATE OR REPLACE FUNCTION public.fn_list_pedidos(
v_id_user pedido.id_user%TYPE
) RETURNS TABLE (id_pedido pedido.id_pedido%TYPE, id_user pedido.id_user%TYPE,id_business pedido.id_business%TYPE,
description pedido.description%TYPE, id_state pedido.id_state%TYPE, total_price pedido.total_price%TYPE, updated_last pedido.updated_last%TYPE,
created pedido.created%TYPE, email pedido.email%TYPE, state state.description%TYPE) AS $$
BEGIN
	RETURN QUERY
	SELECT p.*, s.description FROM pedido p
	INNER JOIN state s ON
	p.id_state = s.id_state
	WHERE p.id_user = v_id_user;
END;
$$LANGUAGE plpgsql;

SELECT * FROM fn_list_pedidos(63);

CREATE OR REPLACE FUNCTION public.fn_list_pedidos_business(
v_id_business pedido.id_business%TYPE
) RETURNS TABLE (id_pedido pedido.id_pedido%TYPE, id_user pedido.id_user%TYPE,id_business pedido.id_business%TYPE,
description pedido.description%TYPE, id_state pedido.id_state%TYPE, total_price pedido.total_price%TYPE, 
updated_last pedido.updated_last%TYPE, created pedido.created%TYPE, email pedido.email%TYPE, state state.description%TYPE) AS $$
BEGIN
	RETURN QUERY
	SELECT p.*, s.description FROM pedido p
	INNER JOIN state s ON
	p.id_state = s.id_state
	WHERE p.id_business = v_id_business;
END;
$$LANGUAGE plpgsql;

SELECT * FROM fn_list_pedidos_business(23);


CREATE OR REPLACE FUNCTION public.fn_add_product(
v_id_business products.id_business%TYPE, v_name products.name%TYPE,
v_price products.price%TYPE, v_url products.url%TYPE)
RETURNS SETOF PRODUCTS AS $$
DECLARE id_created products.id_product%TYPE;
BEGIN
	INSERT INTO PRODUCTS (ID_BUSINESS, NAME, PRICE, URL)
	VALUES (v_id_business, v_name, v_price, v_url)
	RETURNING id_product INTO id_created;
	RETURN QUERY
	SELECT * FROM PRODUCTS WHERE id_product = id_created;
END;
$$LANGUAGE plpgsql;

SELECT * FROM fn_add_product(23, 'Nuevo producto', 11.50, 'sadsadsadasudytsa6drtas67dt');


CREATE OR REPLACE FUNCTION public.fn_list_products(
v_id_business business.id_business%TYPE)
RETURNS SETOF PRODUCTS AS $$
BEGIN
	RETURN QUERY
	SELECT * FROM PRODUCTS WHERE id_business = v_id_business;
END;
$$LANGUAGE plpgsql;

SELECT * FROM fn_list_products(23);

CREATE OR REPLACE FUNCTION public.fn_change_state_order
(v_id_pedido pedido.id_pedido%TYPE, v_id_new_state pedido.id_state%TYPE, v_updated_last pedido.updated_last%TYPE)
RETURNS TABLE (id_pedido pedido.id_pedido%TYPE, id_user pedido.id_user%TYPE,id_business pedido.id_business%TYPE,
description pedido.description%TYPE, id_state pedido.id_state%TYPE, total_price pedido.total_price%TYPE, 
updated_last pedido.updated_last%TYPE, created pedido.created%TYPE, email pedido.email%TYPE, state state.description%TYPE) AS $$
BEGIN
	UPDATE PEDIDO pedido
		SET id_state = v_id_new_state,
		    updated_last = v_updated_last
		WHERE (pedido.id_pedido = v_id_pedido);
	RETURN QUERY
	SELECT p.*, s.description FROM pedido p
	INNER JOIN state s ON
	p.id_state = s.id_state
	WHERE p.id_pedido = v_id_pedido;
END;
$$LANGUAGE plpgsql;

SELECT * FROM fn_change_state_order(45, 7, '2020-06-14 13:34:54');

/****************APUNTES*****************/

/*ALTER TABLE pedido ADD COLUMN id_state INT REFERENCES state;
ALTER TABLE detalle_pedido RENAME article TO description;
ALTER TABLE detalle_pedido RENAME TO pedido;
ALTER TABLE pedido RENAME id_detalle_pedido TO id_pedido;
ALTER TABLE detalle_pedido ADD COLUMN QUANTITY INT NOT NULL DEFAULT 0;


https://www.postgresqltutorial.com/postgresql-alter-table/
*/
