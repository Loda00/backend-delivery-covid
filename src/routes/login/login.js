const express = require('express')
const { compareSync } = require('bcryptjs')
const jwt = require('jsonwebtoken')
const { pool } = require('../../connection/connection')

const app = express()

app.post('/login', async (req, res) => {

  const { email, password } = req.body

  const con = await pool.connect()

  try {
    const reply = await con.query('SELECT * FROM fn_login($1);', [email])

    if (reply.rows.length === 0) {
      return res.status(500)
      .json({
        error: true,
        description: 'Email is wrong !'
      })
    }

    const { v_password, v_email, v_nickname } = reply.rows[0]

    const passwordCompare = compareSync(password, v_password)

    if (!passwordCompare) {
      return res.status(500)
      .json({
        error: true,
        description: 'Password is wrong !'
      })
    }

    const payload = {
      v_password,
      v_email,
      v_nickname,
    }

    const A_DAY = 60 * 60 * 24

    const token = jwt.sign(payload, process.env.SECRET_TOKEN, {
      expiresIn: A_DAY,
    })

    delete reply.rows[0].v_password

    res.json({
      reply: reply.rows[0],
      token,
      rowCount: reply.rowCount,
    })
  } catch (error) {
    throw new Error(error)
  } finally {
    await con.release()
  }
})


module.exports = app
