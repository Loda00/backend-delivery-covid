const express = require('express')
const { pool } = require('../../connection/connection')

const app = express()

app.get('/product/:id', async (req, res) => {
  const { id } = req.params
  const con = await pool.connect()
  try {

    const reply = await con.query('SELECT * FROM fn_list_products($1);', [Number(id)])
    res.json({
      success: true,
      data: reply.rows
    })
  } catch (error) {
    throw new Error(error)
  } finally {
    await con.release()
  }
})

app.post('/product', async (req, res) => {
  const { name, price, id_business } = req.body
  const { filename } = req.file
  console.log('req.params', req.file, req.body)
  const con = await pool.connect()
  try {

    const reply = await con.query('SELECT * FROM fn_add_product($1, $2, $3, $4);', [Number(id_business), name, Number(price), filename])
    res.json({
      success: true,
      data: reply.rows[0]
    })
  } catch (error) {
    throw new Error(error)
  } finally {
    await con.release()
  }
})

module.exports = app
