const express = require('express')
const moment = require('moment')
const { pool } = require('../../connection/connection')
const { mailer } = require('../../utils/nodemailer')

const app = express()

app.post('/order', async (req, res) => {

  const con = await pool.connect()
  const dateCurrent = moment().format('YYYY-MM-DD HH:mm:ss')
  try {
    await con.query('BEGIN')
    const { selectedProducts, idUser, idBusiness, description = '', id_state = 6, total, emailBusiness, emailClient } = req.body
    console.log('resOrder', req.body)

    const replyOrder = await con.query('SELECT fn_add_pedido($1, $2, $3, $4, $5, $6, $7);', 
    [idUser, idBusiness, description, id_state, total, emailClient, dateCurrent])

    if (typeof replyOrder.rows[0].fn_add_pedido !== 'number') {
      await con.query('ROLLBACK')
      return res.status(500).json({
        success: false,
        error: 'Error al crear el pedido'
      })
    }

    let query = 'INSERT INTO detalle_pedido (id_pedido, article, verify, price, unit_price, url, quantity) VALUES'
    selectedProducts.forEach((item, i) => {
      query += `(${replyOrder.rows[0].fn_add_pedido}, '${item.name}', 0, ${item.priceCount}, ${item.price}, 
      '${item.url}', '${item.quantity}')${selectedProducts.length - 1 === i ? ';' : ','}`
    })

    await con.query(query)

    const sendEmail = await mailer('BUY', `${emailBusiness}, ${emailClient}`, { total: total })

    if (sendEmail === 500) {
      await con.query('ROLLBACK')
      return res.status(500).json({
        success: false,
        description: 'Error al enviar los correos'
      })
    }

    await con.query('COMMIT')

    return res.json({
      success: true,
      data: 'Creado el pedido correctamente !'
    })
  } catch (error) {
    await con.query('ROLLBACK')
    throw new Error(error)
  } finally {
    await con.release()
  }

})

app.get('/order-client/:id', async (req, res) => {

  const { id } = req.params

  const con = await pool.connect()
  try {

    if (!Number(id)) {
      return res.json({
        success: false,
        error: 'Param is not number'
      })
    }

    const reply = await con.query('SELECT * FROM fn_list_pedidos($1);', [Number(id)])
    res.json({
      success: true,
      reply: reply.rows
    })

  } catch (error) {
    throw new Error(error)
  } finally {
    await con.release()
  }
})

app.get('/order_business/:id', async (req, res) => {

  const { id } = req.params

  const con = await pool.connect()
  try {

    if (!Number(id)) {
      return res.json({
        success: false,
        error: 'Param is not number'
      })
    }

    const reply = await con.query('SELECT * FROM fn_list_pedidos_business($1);', [Number(id)])

    res.json({
      success: true,
      reply: reply.rows
    })

  } catch (error) {
    throw new Error(error)
  } finally {
    await con.release()
  }
})

app.get('/order-all/:id', async (req, res) => {

  const { id } = req.params

  const con = await pool.connect()
  try {

    if (!Number(id)) {
      return res.json({
        success: false,
        error: 'Param is not number'
      })
    }

    const reply = await con.query('SELECT * FROM fn_list_pedidos_all();')

    res.json({
      success: true,
      reply: reply.rows
    })

  } catch (error) {
    throw new Error(error)
  } finally {
    await con.release()
  }
})

app.put('/order-change-state', async (req, res) => {

  const con = await pool.connect()

  const dateCurrent = moment().format('YYYY-MM-DD HH:mm:ss')
  const { newState, idPedido, emailClient } = req.body

  try {
    await con.query('BEGIN')
    const reply = await con.query('SELECT * FROM fn_change_state_order($1, $2, $3);', [idPedido, newState, dateCurrent])
    console.log('replyreply', reply.rows[0])
    let typeState = ''

    if (newState === 7) {
      typeState = 'STATE SENDED'
    } else if (newState === 8) {
      typeState = 'STATE DELIVERED'
    } else if (newState === 9) {
      typeState = 'STATE CANCELED'
    }

    const sendEmail = await mailer(typeState, emailClient)

    console.log('sendEmailsendEmail', sendEmail)
    if (sendEmail === 500) {
      await con.query('ROLLBACK')
      return res.status(500).json({
        success: false,
        description: 'Error al enviar los correos'
      })
    }

    await con.query('COMMIT')

    res.json({
      success: true,
      reply: reply.rows[0]
    })
  } catch (error) {
    await con.query('ROLLBACK')
    throw new Error(error)
  } finally {
    await con.release()
  }
})

module.exports = app
