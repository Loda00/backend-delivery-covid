const express = require('express')
const app = express()

app.use(
  require('./user/createUser'),
  require('./state'),
  require('./roles/roles'),
  require('./login/login'),
  require('./business'),
  require('./order'),
  require('./detailOrder'),
  require('./product')
)

module.exports = app
