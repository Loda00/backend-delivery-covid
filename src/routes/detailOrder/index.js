const expres = require('express')
const { pool } = require('../../connection/connection')
const app = expres()


app.get('/detailOrder/:id', async (req, res) => {

  const { id } = req.params
  const con = await pool.connect()
  try {

    if (!Number(id)) {
      return res.json({
        success: false,
        error: 'Param is not number'
      })
    }

    const reply = await con.query('SELECT * FROM fn_list_detalle_pedido($1);', [Number(id)])

    res.json({
      success: true,
      reply: reply.rows
    })

  } catch (error) {
    throw new Error(error)
  } finally {
    await con.release()
  }
})

module.exports = app
