const express = require('express')
const jwt = require('jsonwebtoken')
const { hashSync, genSaltSync } = require('bcryptjs')
const { mailer } = require('../../utils/nodemailer')
const { pool } = require('../../connection/connection')

const app = express()

const ID_NOT_CREATED = -1

app.post('/user', async (req, res) => {

  const { email, password, rol = 1, state = 1 } = req.body

  const encryption = genSaltSync(parseInt(process.env.BCRYPT_DIFICULT, 10))
  const hashPassword = hashSync(password, encryption)

  const con = await pool.connect()

  try {
    const regex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (!regex.exec(email)) {
      return res.status(500).json({
        success: false,
        description: 'E-mail not exist !'
      })
    }

    await con.query('BEGIN')
    const reply = await con.query('SELECT fn_add_user($1, $2, $3, $4);', [email, hashPassword, rol, state])

    if (reply.rows[0].fn_add_user === ID_NOT_CREATED) {
      return res.status(500).json({
        success: false,
        description: 'E-mail already exist !'
      })
    }

    const sendEmail = await mailer('SIGNUP', email)

    if (sendEmail === 500) {
      await con.query('ROLLBACK')
      return res.status(500).json({
        success: false,
        description: 'Email not exist !'
      })
    }

    const user  = await con.query('SELECT * FROM users WHERE id_user = $1', [reply.rows[0].fn_add_user])

    const { nickname: v_nickname, password: v_password, email: v_email} = user.rows[0]

    const payload = {
      v_password,
      v_email,
      v_nickname,
    }

    const A_DAY = 60 * 60 * 24

    const token = jwt.sign(payload, process.env.SECRET_TOKEN, {
      expiresIn: A_DAY,
    })

    delete user.rows[0].v_password
   
    const newReply = {
      id: user.rows[0].id_user,
      v_age: user.rows[0].age,
      v_email: user.rows[0].email,
      v_id_rol: user.rows[0].id_rol,
      v_id_state: user.rows[0].id_state,
      v_lastname: user.rows[0].lastname,
      v_names: user.rows[0].names,
      v_nickname: user.rows[0].nickname,
      v_type_business: user.rows[0].id_type_business
    }

    res.json({
      success: 200,
      reply: newReply,
      token,
    })
    await con.query('COMMIT')
  } catch (error) {
    await con.query('ROLLBACK')
    throw new Error(error)
  } finally {
    await con.release()
  }
})

module.exports = app
