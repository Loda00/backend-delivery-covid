const express = require('express')
const { pool } = require('../../connection/connection')

const app = express()

app.post('/business', async (req, res) => {

  const { id_user, name, latitud, longitud, description } = req.body

  const con = await pool.connect()

  try {

    const reply = await con.query('SELECT fn_add_business($1, $2, $3, $4, $5);', [id_user, name, latitud, longitud, description])

    if (reply.rows[0].fn_add_business === -1) {
      return res.status(503).json({
        error: true,
        message: 'Ya tiene un negocio creado'
      })
    }

    // console.log('res', reply.rows[0])

    return res.json(reply.rows)

  } catch (error) {
    throw new Error(error)
  } finally {
    await con.release()
  }
})

app.get('/business', async (req, res) => {

  const con = await pool.connect()

  try {

    const reply = await con.query('SELECT * FROM fn_list_business();')

    // console.log('res', reply.rows)

    return res.json({
      reply: reply.rows,
    })

  } catch (error) {
    throw new Error(error)
  } finally {
    await con.release()
  }
})

module.exports = app

