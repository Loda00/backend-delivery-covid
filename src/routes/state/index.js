const express = require('express')
const { pool } = require('../../connection/connection')

const app = express()

app.get('/state', async (req, res) => {

  const con = await pool.connect()

  try {
    const reply = await con.query('SELECT * FROM fn_list_state();')
    res.json({
      reply: reply.rows,
      rowCount: reply.rowCount,
      fields: reply.fields,
    })
    console.log('reply', reply.rows)
  } catch (error) {
    throw new Error(error)
  } finally {
    await con.release()
  }
})

module.exports = app
