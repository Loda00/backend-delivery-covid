const express = require('express')
const { pool } = require('../../connection/connection')

const app = express()

app.get('/roles', async (req, res) => {

  const con = await pool.connect()

  try {
    const reply = await con.query('SELECT * FROM fn_list_roles();')
    res.json({
      reply: reply.rows,
      rowCount: reply.rowCount,
      fields: reply.fields,
    })
    console.log('reply', reply.rows)
  } catch (error) {
    throw new Error(error)
  } finally {
    await con.release()
  }

  // const client = await pool.connect()

  // try {
  //   await client.query('BEGIN')

  //   await client.query('SELECT fn_insert_test($1)', ['prueba test >w< !'])

  //   await client.query('SELECT fn_insert_role("prueba test 2")')

  //   const reply = await client.query('SELECT * FROM fn_list_roles_2()')

  //   await client.query('COMMIT')
  //   console.log('res', reply.rows)

  //   res.json(reply.rows)

  // } catch (error) {
  //   await client.query('ROLLBACK')
  //   console.log('error', error)
  // } finally {
  //   client.release()
  // }
})

app.get('/test', (req, res) => {
  try {
    res.json({
      data: [],
    })
  } catch (error) {
    console.log('err', error)
  } finally {
    console.log('finnally')
  }
})

module.exports = app
