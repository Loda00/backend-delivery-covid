require('graphql-import-node')
const typeDefs = require('./schema.graphql')
const { GraphQLSchema } = require('graphql')
const { makeExecutableSchema } = require('graphql-tools')
const resolvers = require('../resolvers/resolvers')

const schema = makeExecutableSchema({
  typeDefs,
  resolvers,
})

module.exports = schema
