const query = require('./query')
const type = require('./type')
const mutation = require('./mutation')

const resolvers = {
  ...mutation,
  ...query,
  ...type,
}

module.exports = resolvers
