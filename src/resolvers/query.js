const { ApolloError } = require('apollo-server-express')
const { pool } = require('../connection/connection')
const { compareSync } = require('bcryptjs')
const jwt = require('jsonwebtoken')

const query = {
  Query: {
    getRoles: async () => {

      const con = await pool.connect()
      try {
        const reply = await con.query('SELECT * FROM fn_list_roles();')
        // throw new ApolloError ('Hay un error')
        return reply.rows
      } catch (error) {
        throw new ApolloError(error)
      } finally {
        await con.release()
      }

    },
    getState: async () => {

      const con = await pool.connect()
      try {
        const reply = await con.query('SELECT * FROM fn_list_state();')
        return reply.rows
      } catch (error) {
        throw new ApolloError(error)
      } finally {
        await con.release()
      }

    },
    getTypeBusiness: async () => {

      const con = await pool.connect()
      try {
        const reply = await con.query('SELECT * FROM fn_list_type_business();')
        return reply.rows
      } catch (error) {
        throw new ApolloError(error)
      } finally {
        await con.release()
      }

    },
    login: async (_, { email, password }) => {

      const con = await pool.connect()
      try {
        const reply = await con.query('SELECT * FROM fn_login($1);', [ email ])

        if (reply.rows.length === 0) {
          throw new ApolloError('Email is wrong !')
        }

        const { v_password, v_email, v_nickname } = reply.rows[0]

        const passwordCompare = compareSync(password, v_password)

        if (!passwordCompare) {
          throw new ApolloError('Password is wrong !')
        }

        const payload = {
          v_password,
          v_email,
          v_nickname,
        }

        const A_DAY = 60 * 60 * 24

        const token = jwt.sign(payload, process.env.SECRET_TOKEN, {
          expiresIn: A_DAY,
        })

        reply.rows[0].v_password = ''

        const response = {
          ...reply.rows[0],
          token,
        }
        return response
      } catch (error) {
        throw new ApolloError(error)
      } finally {
        await con.release()
      }

    },
  }
}

module.exports = query
