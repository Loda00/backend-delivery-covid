const { ApolloError, ValidationError } = require('apollo-server-express')
const { hashSync, genSaltSync } = require('bcryptjs')
const { mailer } = require('../utils/nodemailer')
const { pool } = require('../connection/connection')

const mutation = {
  Mutation: {
    addUser: async (_, { user }) => {
      const con = await pool.connect()
      try {
        console.log('user', user)

        const ID_NOT_CREATED = -1
        const regex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

        const { v_email, v_password, v_id_rol = 1, v_id_state } = user

        const encryption = genSaltSync(parseInt(process.env.BCRYPT_DIFICULT, 10))
        const hashPassword = hashSync(v_password, encryption)

        if (!regex.exec(v_email)) {
          throw new ValidationError('Hay un error')
        }

        await con.query('BEGIN')
        const reply = await con.query('SELECT fn_add_user($1, $2, $3, $4);', [v_email, hashPassword, v_id_rol, v_id_state])

        if (reply.rows[0].fn_add_user === ID_NOT_CREATED) {
          throw new ValidationError('E-mail already exist !')
        }
        const sendEmail = await mailer(v_email, v_email.split('@')[0])

        if (sendEmail === 500) {
          throw new ValidationError('Email not exist !')
        }

        await con.query('COMMIT')

        return { id: reply.rows[0].fn_add_user}
      } catch (error) {
        await con.query('ROLLBACK')
        throw new ApolloError(error)
      } finally {
        await con.release()
      }
    }
  }
}

module.exports = mutation
