const express = require('express')
const morgan = require('morgan')
const cors = require('cors')
const helmet = require('helmet')
const multer = require('multer')
const fs = require('fs')
const { v4: uuid } = require('uuid');
const { createServer } = require('http')
const { securityJwt } = require('./utils/jwt.js')
const expressPlayGround = require('graphql-playground-middleware-express').default
const { ApolloServer } = require('apollo-server-express')
const compression = require('compression')
require('dotenv').config()
const path = require('path')
const schema = require('./schemas')

const storage = multer.diskStorage({
  destination: path.join(__dirname, 'public/uploads'),
  filename: (req, file, cb) => {
    cb(null, uuid() + path.extname(file.originalname))
  }
})

const app = express()

let morganValue = 'dev'

if (process.env.NODE_ENV === 'production') {
  morganValue = 'common'
  app.use(compression())
  app.use(helmet())
}

app.use(multer({
  storage,
  dest: path.join(__dirname, 'public/uploads'),
  fileFilter: (req, file, cb) => {
    const fileTypes = /jpge|jpg|png|gif/
    const mimeType = fileTypes.test(file.mimetype)
    const extension = fileTypes.test(path.extname(file.originalname))

  return cb(null, true)
    //   if (mimeType && extension) {
    // } else {
    //   cb('Error : Archivo no tiene un formato válido', false)
    // }
  },
  limits: 1024 * 1024 * 5
}).single('file'))

app.use(morgan(morganValue, fs.createWriteStream(__dirname + '/access.log',{flags: 'a'})))
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(cors())
app.use(express.static(path.join(__dirname, 'public')))
app.use((req, res, next) => {
  if (req.path === '/api/v1/login' || (req.path === '/api/v1/user') && req.method === 'POST' ||
      req.path === '/graphql') {
    return next()
  }
  securityJwt(req, res, next)
})

// routes
app.use('/api/v1',require('./routes'))

const server = new ApolloServer({
  schema,
  introspection: true,
})

server.logger

server.applyMiddleware({ app })

app.get('/', expressPlayGround({
  endpoint: '/graphql',
}))


const http = createServer(app)

http.listen(process.env.PORT, () => {
  console.log('location ...', __filename)
  console.log(`Server running on port http://localhost:${process.env.PORT}/`)
})
